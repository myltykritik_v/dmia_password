import os
import pickle

import daal4py
from flask import Flask, request, render_template, abort

from helpers import make_features, check_password

app = Flask(__name__)
APP_ROOT = os.path.dirname(os.path.abspath(__file__))

with open(f'{APP_ROOT}/inference_data_files/model.pkl', 'rb') as inp:
    lgbm_model = pickle.load(inp)


@app.route('/', methods=['GET', 'POST'])
def hello_world():
    try:
        if request.method == 'GET':
            return render_template('index.html')

        if request.method == 'POST':
            pw = request.form["password"]
            features = make_features(pw)
            lgbm_prediction = daal4py.gbt_regression_prediction( ).compute(
                features, lgbm_model).prediction[0][0]
            lgbm_prediction = f'Good: {lgbm_prediction:.2f}' if lgbm_prediction < 1 else f'Bad: {lgbm_prediction:.2f}'
            password_kwargs = check_password(pw)
            return render_template('index.html', password=pw,
                                   prediction=lgbm_prediction,
                                   **password_kwargs)
    except Exception:
        abort(500)


if __name__ == '__main__':
    app.run(threaded=True)
