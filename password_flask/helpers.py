import os

import enchant
import numpy

APP_ROOT = os.path.dirname(os.path.abspath(__file__))

word_dict = enchant.Dict("en_US")
top10k = set([x.strip( ) for x in open(f'{APP_ROOT}/inference_data_files/top10k.txt', 'r').readlines( )])
top1m = set([x.strip( ) for x in open(f'{APP_ROOT}/inference_data_files/top1m.txt', 'r').readlines( )])


def make_features(password):
    x0 = word_dict.check(password) * 1 if password.isalpha( ) else 0
    x1 = 1 if password in top10k else 0
    x2 = 1 if password in top1m else 0
    return numpy.array([[x0, x1, x2]])


def check_password(password):
    check_kwargs = {}
    if password == password.lower( ) or password == password.upper( ):
        check_kwargs['register'] = 'Bad'
    else:
        check_kwargs['register'] = 'Good'
    if password.isalnum( ):
        check_kwargs['special'] = 'Bad'
    else:
        check_kwargs['special'] = 'Good'
    if any(char.isdigit( ) for char in password):
        check_kwargs['numbers'] = 'Good'
    else:
        check_kwargs['numbers'] = 'Bad'
    if any(char.isalpha( ) for char in password):
        check_kwargs['letters'] = 'Good'
    else:
        check_kwargs['letters'] = 'Bad'
    return check_kwargs
